package com.softserve.edu.app;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String answer;

        Scanner sc = new Scanner(System.in);

        System.out.println("============ Envelope analyzer =================");

        do {
            try {
                System.out.println("type a");
                int a = Integer.parseInt(sc.nextLine());
                System.out.println("type b");
                int b = Integer.parseInt(sc.nextLine());

                Envelope first = new Envelope(a, b);

                System.out.println("type c");
                int c = Integer.parseInt(sc.nextLine());
                System.out.println("type d");
                int d = Integer.parseInt(sc.nextLine());

                Envelope second = new Envelope(c, d);

                System.out.println("Envelope with sides a,b " +
                        (first.canContain(second) ? "can " : "cannot ") + "contain envelope with sides c,d");

                System.out.println("Type 'y' or 'yes' tp continue");
                answer = sc.next();
            } catch(InputMismatchException | IllegalArgumentException e) {
                System.out.println("wrong input");
                answer = "yes";
            }
        } while (answer.equalsIgnoreCase("yes") || answer.equalsIgnoreCase("y"));

    }

}
