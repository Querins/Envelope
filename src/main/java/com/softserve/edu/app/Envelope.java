package com.softserve.edu.app;

public class Envelope {

    private int a;
    private int b;

    public Envelope(int a, int b) {
        setA(a);
        setB(b);
    }

    public void setA(int a) {
        validate(a);
        this.a = a;
    }

    public void setB(int b) {
        validate(b);
        this.b = b;
    }

    private static void validate(int n) {
        if(n < 0) {
            throw new IllegalArgumentException("Side widths must be positive");
        }
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    private int area() {
        return a*b;
    }

    public boolean canContain(Envelope env) {
        return env.getA() <= getA() && env.getB() <= getB();
    }

}
