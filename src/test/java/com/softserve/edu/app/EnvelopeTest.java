package com.softserve.edu.app;

import org.junit.Test;

import static org.junit.Assert.*;

public class EnvelopeTest {
    @org.junit.Test
    public void canContain_larger() {
        assertTrue(new Envelope(10,10).canContain(new Envelope(5,5)));
    }

    @org.junit.Test
    public void canContain_smaller() {
        assertFalse(new Envelope(5,5).canContain(new Envelope(10,10)));
    }

    @org.junit.Test
    public void canContain_smaller_1() {
        assertFalse(new Envelope(5,10).canContain(new Envelope(10,10)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void wrongArgs() {
        new Envelope(-9,0);
    }

}